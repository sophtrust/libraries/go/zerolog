# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)
and utilizes [Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/) messages.

## Unreleased

No unreleased changes

## v1.26.2 (2022-01-12)

### Notes

* Applied patches from upstream `github.com/rs/zerolog` repository.
  
## v1.26.0 (2021-09-09)

### Fixes

* Fixed issue with `CallerAndFunction()` context not adding caller information.

### Enchancements

* Updated default `FunctionMarshalFunc()` to use a double colon (`::`) between the package and function name instaed of a single colon (`:`).

## v1.25.1 (2021-09-04)

### Enchancements

* Updated `ParseLogrusMessages()` to accept a new error handler function which is called if there is an error while parsing a logrus message.

## v1.25.0 (2021-09-02)

### Enchancements

* Replaced `PackageCaller()` from context with `Function()` function. `Function()` acts exactly like `Caller()` but logs the package and function name instead of file and line.
* Added `CallerAndFunction()` to context which adds both the file and line as well as the function to the context.

## v1.24.1 (2021-09-01)

### Enchancements

* Added `WithContext()` function to global log object.
  
## v1.24.0 (2021-08-23)

### Notes

* Initial fork from `v1.23.0` tag of `github.com/rs/zerolog` repository
