package zerolog

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"sort"
	"strings"
	"sync"
	"time"
)

const (
	defaultMaxFiles = 5
	defaultMaxBytes = 1024 * 1024 * 5 // 5MB
)

// FileWriter implements the io.Writer interface and writes the log file to the given path using the prefix and
// extension for a file name.
//
// The "active" log file will simply be named with the prefix followed by the extension. Rotated log files will
// be the prefix followed by an underscore (_) and a nano-timestamp followed by the extension.
//
// Files will be no larger than MaxBytes in size and no more than MaxFiles will be present at any time. MaxFiles
// indicates the total number of rotated files plus the active log file.
type FileWriter struct {
	// MaxFiles is the maximum number of log files to be present in the log folder, including the active log file.
	MaxFiles int

	// MaxBytes indicates the maximum size of the log file in bytes. Files will be rotated when the message being
	// written will exceed this size.
	MaxBytes uint64

	// FolderPerm represents the permissions to set on any folders created when creating the log folder.
	FolderPerm fs.FileMode

	// FilePerm represents the permissions to set when creating log files.
	FilePerm fs.FileMode

	path        string
	prefix      string
	ext         string
	current     *os.File
	currentSize uint64
	sync.Mutex
}

// NewFileWriter creates a new FileWriter object and initializes it.
//
// The files will be created in the path specified and each file will start with the given prefix. The given file
// extension will be added to the end of the file name.
func NewFileWriter(path, prefix, ext string) *FileWriter {
	if ext != "" && ext[0] != '.' {
		ext = fmt.Sprintf(".%s", ext)
	}
	return &FileWriter{
		path:       path,
		prefix:     prefix,
		ext:        ext,
		MaxFiles:   defaultMaxFiles,
		MaxBytes:   defaultMaxBytes,
		FolderPerm: os.FileMode(0755),
		FilePerm:   os.FileMode(0644),
	}
}

// Write writes p to the current file, then checks to see if rotation is necessary.
func (w *FileWriter) Write(p []byte) (n int, err error) {
	w.Lock()
	defer w.Unlock()

	// make sure the file is open before attmepting to write
	if w.current == nil {
		if err := w.init(); err != nil {
			return 0, err
		}
	}

	// rotate the file if this write will exceed the size
	if (w.currentSize + uint64(len(p))) >= w.MaxBytes {
		if err := w.rotate(); err != nil {
			return 0, err
		}
	}

	// write the data and then rotate if necessary
	n, err = w.current.Write(p)
	w.currentSize += uint64(n)
	if err != nil {
		return n, err
	}
	return n, nil
}

// Close closes the current file.
func (w *FileWriter) Close() error {
	w.Lock()
	defer w.Unlock()

	// file is already closed
	if w.current == nil {
		return nil
	}

	// close the file handle
	if err := w.current.Close(); err != nil {
		return err
	}
	w.current = nil
	return nil
}

// init creates the file path if necessary and opens the file.
func (w *FileWriter) init() error {
	fi, err := os.Stat(w.path)
	if err != nil && os.IsNotExist(err) {
		err := os.MkdirAll(w.path, w.FolderPerm)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	} else if !fi.IsDir() {
		return errors.New("log file path must be a directory")
	}
	return w.open()
}

// open simply opens up the current log file for writing.
func (w *FileWriter) open() error {
	file := path.Join(w.path, fmt.Sprintf("%s%s", w.prefix, w.ext))
	var err error
	w.current, err = os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, w.FilePerm)
	if err != nil {
		return err
	}
	fileInfo, err := w.current.Stat()
	if err != nil {
		return err
	}
	w.currentSize = uint64(fileInfo.Size())
	return nil
}

// rotate closes the current file and rotates out the previous log files.
func (w *FileWriter) rotate() error {
	if err := w.current.Close(); err != nil {
		return err
	}
	src := path.Join(w.path, fmt.Sprintf("%s%s", w.prefix, w.ext))
	dest := path.Join(w.path, fmt.Sprintf("%s_%d%s", w.prefix, time.Now().UnixNano(), w.ext))
	if err := os.Rename(src, dest); err != nil {
		return err
	}
	if err := w.clean(); err != nil {
		return err
	}
	return w.open()
}

// clean removes any old log files.
func (w *FileWriter) clean() error {
	d, err := os.Open(w.path)
	if err != nil {
		return err
	}
	names, err := d.Readdirnames(0)
	if err != nil {
		return err
	}

	var filenames []string
	for _, n := range names {
		if strings.HasPrefix(n, w.prefix+"_") {
			filenames = append(filenames, n)
		}
	}
	if len(filenames) <= w.MaxFiles {
		return nil
	}

	sort.Strings(filenames)
	toDel := filenames[0 : len(filenames)-w.MaxFiles+1]
	for _, n := range toDel {
		if err := os.Remove(path.Join(w.path, n)); err != nil {
			return err
		}
	}
	return nil
}
