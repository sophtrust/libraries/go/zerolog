package log

import (
	"os"
	"sync"

	"go.sophtrust.dev/pkg/zerolog"
)

var (
	mutex sync.RWMutex

	// Logger is the global logger.
	Logger = zerolog.New(os.Stderr).With().Timestamp().Logger()
)
