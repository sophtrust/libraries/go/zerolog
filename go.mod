module go.sophtrust.dev/pkg/zerolog

go 1.16

require (
	github.com/coreos/go-systemd/v22 v22.3.2
	github.com/pkg/errors v0.9.1
	github.com/rs/xid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/tools v0.1.5
)
